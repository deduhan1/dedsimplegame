﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class Parallax : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private float _speed;

    private RawImage _image;

    private void Start()
    {
        if (_camera == null)
            _camera = FindObjectOfType<Camera>();

        _image = GetComponent<RawImage>();
    }

    private void Update()
    {
        _image.uvRect = new Rect(_camera.transform.position.x * _speed, 0, _image.uvRect.width, _image.uvRect.height);
    }
}
