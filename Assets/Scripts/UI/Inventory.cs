﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Bag _bag;
    [SerializeField] private InventoryCell _template;
    [SerializeField] private Transform _cellsContainer;
    [SerializeField] private Canvas _canvas;

    private List<InventoryCell> _cells;

    public Bag Bag => _bag;

    private void Awake()
    {
        _cells = new List<InventoryCell>();
    }

    private void OnEnable()
    {
        if (_bag != null)
            _bag.ItemAdded += Add;

        Reload();
    }

    private void OnDisable()
    {
        if (_bag != null)
            _bag.ItemAdded -= Add;
    }

    private void Start()
    {
        Reload();
    }

    public void Init(Bag bag)
    {
        _bag = bag;
    }

    public void Reload()
    {
        Clear();

        RenderCells();
        for (int i = 0; i < _bag.Count(); i++)
            Add(_bag.GetItemOfIndex(i));
    }

    public void RemoveItem(CellViewer itemInfo)
    {
        _bag.RemoveItem(itemInfo);
    }

    private void Add(CellViewer itemInfo)
    {
        if (itemInfo.Item == null)
            return;

        _cells[itemInfo.Position].AddItem(itemInfo);
    }

    private void RenderCells()
    {
        for (int i = 0; i < _bag.Capacity; i++)
        {
            var cell = Instantiate(_template, _cellsContainer.transform);
            cell.Init(i, _canvas.transform, this);
            cell.Ejecting += Eject;
            cell.Swapping += Swap;
            cell.Moving += Move;

            _cells.Add(cell);
        }
    }

    private void Clear()
    {
        foreach (InventoryCell cell in _cells)
            Destroy(cell.gameObject);

        _cells.Clear();
    }

    public bool Inject(InventoryCell cell)
    {
        return _bag.TryAddItem(cell.CellViewer.Contains(), cell.Position);
    }

    private void Eject(InventoryCell cell)
    {
        _bag.DropItem(cell.CellViewer);
        cell.Clear();
    }

    private void Move(InventoryCell cell, InventoryCell targetCell)
    {
        if (_cells.Contains(cell))
        {
            targetCell.Inventory.Bag.ChangePosition(cell.CellViewer, targetCell.Position);
        }
        else
        {
            targetCell.Inventory.Bag.AddCellViewer(cell.CellViewer, targetCell.Position);
            cell.Inventory.Bag.RemoveItem(cell.CellViewer);
        }

        cell.Clear();
    }

    private void Swap(InventoryCell cell, InventoryCell targetCell)
    {
        CellViewer cellViewer = targetCell.CellViewer;

        if (_cells.Contains(cell))
        {
            cell.Inventory.Bag.ChangePosition(cell.CellViewer, targetCell.Position);
            targetCell.Inventory.Bag.ChangePosition(cellViewer, cell.Position);
        }
        else
        {
            targetCell.Inventory.Bag.RemoveItem(targetCell.CellViewer);
            targetCell.Inventory.Bag.AddCellViewer(cell.CellViewer, targetCell.Position);
            cell.Inventory.Bag.RemoveItem(cell.CellViewer);
            cell.Inventory.Bag.AddCellViewer(cellViewer, cell.Position);
        }
    }
}
