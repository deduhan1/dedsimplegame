﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections.Generic;

public class DraggingImage : MonoBehaviour
{
    public InventoryCell InventoryCell { get; private set; }

    public void Init(InventoryCell inventoryCell)
    {
        InventoryCell = inventoryCell;
        GetComponent<Image>().sprite = InventoryCell.CellViewer.Contains().gameObject.GetComponent<SpriteRenderer>().sprite;
    }
}
