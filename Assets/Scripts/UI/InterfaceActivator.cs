﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceActivator : MonoBehaviour
{
    [SerializeField] private GameObject _playerInventory;

    private PlayerInput _input;

    private void Awake()
    {
        _input = new PlayerInput();
    }

    private void OnEnable()
    {
        _input.Enable();
        _input.UI.Inventory.performed += ctx => OnInventoryButton();
    }

    private void OnDisable()
    {
        _input.Disable();
        _input.UI.Inventory.performed -= ctx => OnInventoryButton();
    }

    private void Start()
    {
        Manual();
    }

    private void OnInventoryButton()
    {
        _playerInventory.SetActive(!_playerInventory.activeSelf);
    }

    private void Manual()
    {
        Debug.Log("Press 'I' to open inventory\n");
    }
}
