﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class InventoryCell : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
{
    [SerializeField] private Image _image;
    [SerializeField] private TMP_Text _text;
    [SerializeField] private DraggingImage _template;

    private DraggingImage _draggingImage;
    private Transform _originalParent;
    private Transform _draggingParent;

    public event UnityAction<InventoryCell> Ejecting;
    public event UnityAction<InventoryCell, InventoryCell> Swapping;
    public event UnityAction<InventoryCell, InventoryCell> Moving;

    public CellViewer CellViewer { get; private set; }
    public Inventory Inventory { get; private set; }
    public int Position { get; private set; }

    public void Init(int position, Transform draggingParent, Inventory inventory)
    {
        Position = position;
        Inventory = inventory;
        _originalParent = transform.parent;
        _draggingParent = draggingParent;
    }

    public void AddItem(CellViewer itemInfo)
    {
        CellViewer = itemInfo;

        Render(CellViewer.Contains().gameObject.GetComponent<SpriteRenderer>().sprite, new Color(1, 1, 1, 1), CellViewer.Count().ToString());
    }

    public void Clear()
    {
        CellViewer = null;

        Render(null, new Color(1, 1, 1, 0), "");
    }

    public bool IsEmpty()
    {
        return CellViewer == null ? true : false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (CellViewer == null)
            return; 

        _draggingImage = Instantiate(_template, _draggingParent).GetComponent<DraggingImage>();
        _draggingImage.Init(this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (CellViewer == null)
            return;

        _draggingImage.transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(_draggingImage != null)
            Destroy(_draggingImage.gameObject);
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.TryGetComponent<InventoryCell>(out InventoryCell cell))
        {
            if (cell.CellViewer.Item == null)
                return;

            if (CellViewer == null)
            {
                Moving?.Invoke(cell, this);
            }
            else
            {
                Swapping?.Invoke(cell, this);
            }

            return;
        }

        if (In((RectTransform)_originalParent) == false && CellViewer != null)
        {
            Ejecting?.Invoke(this);
        }
    }

    private void Render(Sprite image, Color imageColor, string count)
    {
        _image.sprite = image;
        _image.color = imageColor;
        _text.text = count;
    }

    private bool In(RectTransform rect)
    {
        return RectTransformUtility.RectangleContainsScreenPoint(rect, _draggingImage.transform.position);
    }
}
