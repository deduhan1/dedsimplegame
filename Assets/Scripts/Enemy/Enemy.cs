﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _maxHealth = 100;

    public Transform eye;
    public Player target;
    public Direction direction;

    private float _currentHealth;

    private void Start()
    {
        direction = new Direction(gameObject, Direction.State.Right);

        _currentHealth = _maxHealth;

        if (target == null)
            target = FindObjectOfType<Player>();
    }

    private void Update()
    {
        direction.ChangeDirectionState(target.transform.position - transform.position);
    }

    public void ApplyDamage(int damage)
    {
        _currentHealth -= damage;

        if (_currentHealth <= 0)
            Destroy(gameObject);
    }
}
