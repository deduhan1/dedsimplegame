﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleEnemyState : State
{
    private Animator _animator;
    private PhysicsMovement _physicsMovement;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Update()
    {
        _physicsMovement.AddForceX(Vector2.zero);
    }

    private void OnEnable()
    {
        _animator.Play("Idle");
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }
}
