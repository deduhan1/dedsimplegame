﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttackEnemyState : State
{
    [SerializeField] private float _delay = 1f;
    [SerializeField] private GameObject _template;
    [SerializeField] private Transform _spawnPoint;

    private Animator _animator;
    private PhysicsMovement _physicsMovement;
    private Player _target;
    private Direction _direction;
    private float _angelInDegrees = 45f;
    private float _timeToAttack = 0f;

    private void Awake()
    {
        _target = GetComponent<Enemy>().target;
        _animator = GetComponent<Animator>();
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Start()
    {
        _direction = GetComponent<Enemy>().direction;
    }

    private void Update()
    {
        _physicsMovement.AddForceX(Vector2.zero);

        _timeToAttack -= Time.deltaTime;

        if (_timeToAttack < 0)
        {
            _animator.Play("RangedAttack");
            _timeToAttack = _delay;
        }
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }

    public void Shot()
    {
        Vector2 fromTo = _target.transform.position - _spawnPoint.transform.position;

        float x = new Vector2(fromTo.x, 0).magnitude;
        float y = fromTo.y;

        float angelInRadians = _angelInDegrees * Mathf.PI / 180;

        float v = Mathf.Sqrt(Mathf.Abs((Physics.gravity.y * x * x) / (2 * (y - Mathf.Tan(angelInRadians) * x) * Mathf.Pow(Mathf.Cos(angelInRadians), 2))));

        var bullet = Instantiate(_template, _spawnPoint.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(_direction.CurrentState == Direction.State.Right ? 1 : -1, 1).normalized * v;
        bullet.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-150f, -400f) * (_direction.CurrentState == Direction.State.Right ? 1 : -1);
    }
}
