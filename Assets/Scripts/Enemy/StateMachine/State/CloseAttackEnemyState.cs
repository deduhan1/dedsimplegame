﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseAttackEnemyState : State
{
    [SerializeField] private float _delay = 1f;
    [SerializeField] private int _damage;

    private Animator _animator;
    private PhysicsMovement _physicsMovement;
    private Player _target;
    private float _timeToAttack = 0f;

    private void Awake()
    {
        _target = GetComponent<Enemy>().target;
        _animator = GetComponent<Animator>();
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Update()
    {
        _physicsMovement.AddForceX(Vector2.zero);

        _timeToAttack -= Time.deltaTime;

        if(_timeToAttack < 0)
        {
            _animator.Play("CloseAttack");
            _timeToAttack = _delay;
        }
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }

    public void CloseAttack()
    {
        _target.ApplyDamage(_damage);
    }
}
