﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleEnemyTransition : Transition
{
    [SerializeField] private float _distanceTransition = 5f;

    private Player _target;

    private void Awake()
    {
        _target = GetComponent<Enemy>().target;
    }

    private void Update()
    {
        if (Vector2.Distance(_target.transform.position, transform.position) >= _distanceTransition)
            NeedTransit = true;
    }
}
