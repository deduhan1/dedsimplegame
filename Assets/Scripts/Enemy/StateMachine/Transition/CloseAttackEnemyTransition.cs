﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseAttackEnemyTransition : Transition
{
    [SerializeField] private float _distanceTransition = 1.5f;

    private Player _target;

    private void Awake()
    {
        _target = GetComponent<Enemy>().target;
    }

    private void Update()
    {
        if (Vector2.Distance(transform.position, _target.transform.position) < _distanceTransition)
            NeedTransit = true;
    }
}
