﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttackEnemyTransition : Transition
{
    [SerializeField] private float _minDistanceTransition = 1.5f;
    [SerializeField] private float _distanceTransition = 5f;

    private Player _target;

    private void Awake()
    {
        _target = GetComponent<Enemy>().target;
    }

    private void Update()
    {
        float distanceToTarget = Vector2.Distance(transform.position, _target.transform.position);

        if (distanceToTarget < _distanceTransition && distanceToTarget > _minDistanceTransition)
            NeedTransit = true;
    }
}
