﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhysicsMovement))]
[RequireComponent(typeof(Animator))]
public abstract class State : MonoBehaviour
{
    [SerializeField] private List<Transition> _transition;

    public void Enter()
    {
        if(enabled == false)
        {
            enabled = true;
            foreach(var transition in _transition)
            {
                transition.enabled = true;
            }
        }
    }

    public void Exit()
    {
        if(enabled == true)
        {
            foreach (var transition in _transition)
                transition.enabled = false;

            enabled = false;
        }
    }

    public State GetNextState()
    {
        foreach(var transition in _transition)
        {
            if (transition.NeedTransit)
                return transition.TargetState;
        }

        return null;
    }
}
