﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PhysicsMovement))]
public class Player : MonoBehaviour
{
    [SerializeField] private int _maxHealth = 100;
    [SerializeField] private Bag _bag;

    public Direction direction;
    
    private PhysicsMovement _physicsMovement;
    private int _currentHealth;

    public event UnityAction<int, int> HealthChanged;

    private void Awake()
    {
        _physicsMovement = GetComponent<PhysicsMovement>();
        direction = new Direction(gameObject, Direction.State.Right);

        _currentHealth = _maxHealth;
    }

    private void Update()
    {
        direction.ChangeDirectionState(_physicsMovement.Velocity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<ItemView>(out ItemView item))
        {
            if (_bag.TryAddItem(item))
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    public void ApplyDamage(int damage)
    {
        _currentHealth -= damage;
        HealthChanged?.Invoke(_currentHealth, _maxHealth);

        if(_currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
}
