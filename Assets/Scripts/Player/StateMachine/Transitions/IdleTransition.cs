﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleTransition : Transition
{
    private PlayerInput _input;
    private PhysicsMovement _physicsMovement;

    private void Start()
    {
        _physicsMovement = GetComponent<PhysicsMovement>();
        _input = new PlayerInput();
        _input.Enable();
    }

    private void Update()
    {
        if (_input.Player.Move.ReadValue<Vector2>() == Vector2.zero && _physicsMovement.Grounded == true)
            NeedTransit = true;
    }
}
