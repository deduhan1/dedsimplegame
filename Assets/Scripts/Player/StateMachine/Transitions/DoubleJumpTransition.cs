﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpTransition : Transition
{
    private PlayerInput _input;

    private void Start()
    {
        _input = new PlayerInput();
        _input.Enable();
        _input.Player.Jump.performed += ctx => TransitToJumpState();
    }

    private void TransitToJumpState()
    {
        NeedTransit = true;
    }
}
