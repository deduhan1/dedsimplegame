﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTransition : Transition
{
    private PhysicsMovement _physicsMovement;

    private void Start()
    {
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Update()
    {
        if (_physicsMovement.Velocity.y < 0 && _physicsMovement.Grounded == false)
            NeedTransit = true;
    }
}
