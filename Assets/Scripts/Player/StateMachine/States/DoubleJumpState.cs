﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpState : State
{
    [SerializeField] private float _doubleJumpForce;

    private Animator _animator;
    private PhysicsMovement _physicsMovement;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Jump()
    {
        _physicsMovement.AddForceY(_doubleJumpForce);
    }

    private void OnEnable()
    {
        _animator.Play("DoubleJump");
        Jump();
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }

}
