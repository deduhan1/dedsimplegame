﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : State
{
    [SerializeField] private float _speed;

    private Animator _animator;
    private PlayerInput _input;
    private PhysicsMovement _physicsMovement;

    private void Awake()
    {
        _physicsMovement = GetComponent<PhysicsMovement>();
        _animator = GetComponent<Animator>();

        _input = new PlayerInput();
        _input.Enable();
    }

    private void OnEnable()
    {
        _animator.Play("Move");
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }

    private void Update()
    {
        Vector2 moveDirection = _input.Player.Move.ReadValue<Vector2>();

        _physicsMovement.AddForceX(moveDirection * _speed);
    }
}
