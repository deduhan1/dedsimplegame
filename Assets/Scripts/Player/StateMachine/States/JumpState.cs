﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpState : State
{
    [SerializeField] private float _jumpForce;

    private Animator _animator;
    private PhysicsMovement _physicsMovement;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _physicsMovement = GetComponent<PhysicsMovement>();
    }

    private void Jump()
    {
        _physicsMovement.AddForceY(_jumpForce);
    }

    private void OnEnable()
    {
        _animator.Play("Jump");
        Jump();
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }
}
