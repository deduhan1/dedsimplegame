﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{
    [SerializeField] private float _XOffset = 2f;
    [SerializeField] private float _YOffset = 2f;
    [SerializeField] private float _smooth = 2f;

    private Player _player;

    private void Awake()
    {
        _player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        Vector3 position = new Vector3(_player.transform.position.x + (_player.direction.CurrentState == Direction.State.Left ? -_XOffset : _XOffset),
            _player.transform.position.y + _YOffset, -10);

        transform.position = Vector3.Lerp(transform.position, position, _smooth * Time.deltaTime);
    }
}
