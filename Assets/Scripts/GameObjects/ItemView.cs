﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ItemData
{
    public string name;
    public string description;
    public int price;

    public static bool operator ==(ItemData first, ItemData second)
    {
        bool nameCompare = first.name == second.name;
        bool descriptionCompare = first.description == second.description;
        bool priceCompare = first.price == second.price;

        return nameCompare && descriptionCompare && priceCompare;
    }
    public static bool operator !=(ItemData first, ItemData second)
    {
        bool nameCompare = first.name != second.name;
        bool descriptionCompare = first.description != second.description;
        bool priceCompare = first.price != second.price;

        return nameCompare || descriptionCompare || priceCompare;
    }
}

public class ItemView : MonoBehaviour
{
    public ItemData itemData = default;
}
