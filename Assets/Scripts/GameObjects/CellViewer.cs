﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CellViewer
{
    public List<ItemView> Item;
    public int Position;
    
    public CellViewer(ItemView item, int position)
    {
        Item = new List<ItemView>();
        Item.Add(item);
        Position = position;
    }

    public void AddItem(ItemView item)
    {
        Item.Add(item);
    }

    public ItemView Contains()
    {
        return Item[0];
    }

    public int Count()
    {
        return Item.Count;
    }
}
