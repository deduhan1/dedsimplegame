﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private int _damage = 2;
    [SerializeField] private float _timeToDestroy = 3f;
    [SerializeField] private float _explosionRadius = 0.5f;

    private float _leftTime = 0f;

    private void Update()
    {
        _leftTime += Time.deltaTime;

        if(_leftTime >= _timeToDestroy)
            StartCoroutine(Explosion());
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 8)
            StartCoroutine(Explosion());
    }

    private IEnumerator Explosion()
    {
        Player player = TryFindPlayer();

        GetComponent<Animator>().Play("BombExplosion");
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        if (player != null)
            player.ApplyDamage(_damage);

        yield return new WaitForSeconds(0.5833f);

        Destroy(gameObject);
    }

    private Player TryFindPlayer()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);

        foreach (Collider2D collider in hitColliders)
            if (collider.gameObject.TryGetComponent<Player>(out Player player))
                return player;

        return null;
    }
}
