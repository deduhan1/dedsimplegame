﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    [SerializeField] private Bag _bag;
    [SerializeField] private Inventory _inventory;

    public void AddToBox(ItemView item, int count)
    {
        _bag.ExpandOn(1);
        _bag.TryAddItem(item, count);
    }
}
