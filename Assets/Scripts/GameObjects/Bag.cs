﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Bag : MonoBehaviour
{
    [SerializeField] private int _capacity;
    [SerializeField] private Transform _container;
    [SerializeField] private ItemEjector _itemEjector;

    [SerializeField] private List<CellViewer> _items;

    public event UnityAction<CellViewer> ItemAdded;

    public int Capacity => _capacity;

    private void Awake()
    {
        _items = new List<CellViewer>();
    }

    public bool TryAddItem(ItemView item, int position = -1)
    {
        if (_items.Count == _capacity && _items.Any(itemInfo => itemInfo.Contains().itemData == item.itemData) == false)
            return false;

        CellViewer cellViewer;

        if (_items.Any(itemInfo => itemInfo.Contains().itemData == item.itemData) && position == -1)
        {
            cellViewer = _items.Find(x => x.Contains().itemData == item.itemData);
            cellViewer.AddItem(item);
        }
        else
        {
            cellViewer = new CellViewer(item, position);
            _items.Add(cellViewer);

            if (position == -1)
            {
                for (int i = 0; i < _capacity; i++)
                {
                    if (_items.Any(itemInfo => itemInfo.Position == i) == false)
                    {
                        cellViewer.Position = i;
                        break;
                    }
                }
            }
        }

        item.transform.parent = _container.transform;
        ItemAdded?.Invoke(cellViewer);

        return true;
    }

    public void AddCellViewer(CellViewer cellViewer, int position)
    {
        cellViewer.Position = position;
        _items.Add(cellViewer);
        ItemAdded?.Invoke(cellViewer);
    }

    public void ChangePosition(CellViewer cellViewer, int position)
    {
        cellViewer.Position = position;
        ItemAdded?.Invoke(cellViewer);
    }

    public void RemoveItem(CellViewer cellViewer)
    {
        _items.Remove(cellViewer);
    }

    public void DropItem(CellViewer cellViewer)
    {
        RemoveItem(cellViewer);
        //_itemEjector.EjectFromInventory(itemInfo, Vector2.right, 2f);
    }

    public int Count()
    {
        return _items.Count;
    }

    public CellViewer GetItemOfIndex(int index)
    {
        return _items[index];
    }

    public void ExpandOn(int value)
    {
        _capacity = value;
    }
}
