﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Chest : MonoBehaviour
{
    [SerializeField] private Bag _bag;
    [SerializeField] private Inventory _gameObjectInventory;

    private Animator _animator;
    private bool _isOpen = false;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out Player player))
            if (player == null)
                return;

        if (_gameObjectInventory.gameObject.activeSelf == false)
        {
            _animator.Play("ChestOpening");
            _gameObjectInventory.Init(_bag);
            _gameObjectInventory.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out Player player))
            if (player == null)
                return;

        _animator.Play("ChestClosing");
        _gameObjectInventory.gameObject.SetActive(false);
    }
}
