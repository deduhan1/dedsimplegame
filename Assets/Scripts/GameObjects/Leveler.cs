﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leveler : MonoBehaviour
{
    [SerializeField] private float _objectHeightAboveGround = .75f;

    private void Start()
    {
        Align();

        Destroy(this);
    }

    private void Align()
    {
        gameObject.transform.position = new Vector2(gameObject.transform.position.x, CalculateDistance(gameObject.transform.position));
    }

    private float CalculateDistance(Vector2 point)
    {
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 2f, 1 << 8);

        return hit ? transform.position.y - (hit.distance - _objectHeightAboveGround) : point.y;
    }
}
