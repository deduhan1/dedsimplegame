﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEjector : MonoBehaviour
{
    [SerializeField] private ItemBox _template;

    public void EjectFromInventory(ItemView item, int count, Vector3 direction, float range)
    {
        RaycastHit2D ray = Physics2D.Raycast(transform.position + direction.normalized * range, Vector2.zero);

        if(ray.collider != null && ray.collider.gameObject.TryGetComponent<ItemBox>(out ItemBox itemBox))
        {
            itemBox.AddToBox(item, count);
            return;
        }

        var newItemBox = Instantiate(_template, transform.position + direction.normalized * range, Quaternion.identity);
        newItemBox.transform.parent = null;
        newItemBox.AddToBox(item, count);
    }
}
