﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direction
{
    public enum State
    {
        Left,
        Right
    }

    private State _state;
    private GameObject _character;

    public State CurrentState => _state;

    public Direction(GameObject character, State startState)
    {
        _character = character;
        _state = startState;
    }

    public void ChangeDirectionState(Vector2 direction)
    {
        if (direction.x < 0 && _state == State.Right)
        {
            _character.transform.localScale = new Vector3(-_character.transform.localScale.x, _character.transform.localScale.y, _character.transform.localScale.z);
            _state = State.Left;
        }
        else if (direction.x > 0 && _state == State.Left)
        {
            _character.transform.localScale = new Vector3(-_character.transform.localScale.x, _character.transform.localScale.y, _character.transform.localScale.z);
            _state = State.Right;
        }
    }
}
